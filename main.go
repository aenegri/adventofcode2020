package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/aenegri/adventofcode2020/aoc2020"
)

// Solver solves an advent of code question
type Solver func()

func main() {
	solvers := map[int]Solver{
		1:  aoc2020.Day1,
		2:  aoc2020.Day2,
		3:  aoc2020.Day3,
		4:  aoc2020.Day4,
		5:  aoc2020.Day5,
		6:  aoc2020.Day6,
		7:  aoc2020.Day7,
		8:  aoc2020.Day8,
		9:  aoc2020.Day9,
		10: aoc2020.Day10,
		11: aoc2020.Day11,
		12: aoc2020.Day12,
		13: aoc2020.Day13,
		14: aoc2020.Day14,
		15: aoc2020.Day15,
		16: aoc2020.Day16,
		17: aoc2020.Day17,
		18: aoc2020.Day18,
	}

	var day int

	flag.IntVar(&day, "day", 1, "day to solve for")
	flag.Parse()

	s, ok := solvers[day]

	if !ok {
		panic(fmt.Sprintf("No solution found for day: %d", day))
	}

	fmt.Printf("***Day %d***\n", day)

	start := time.Now()
	s()

	fmt.Printf("Runtime: %s\n", time.Now().Sub(start))
}
