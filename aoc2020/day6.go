package aoc2020

import (
	"fmt"
	"strings"
)

// Day6 solves day 6
func Day6() {
	text := readFile("input/day6.txt")
	part1 := sumAllQuestions(text, getCountForGroup)
	part2 := sumAllQuestions(text, getCountForGroupV2)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func sumAllQuestions(text string, getGroupCount func(members int, questionCount []int) int) int {
	groups := strings.Split(text, "\n\n")
	count := 0
	for _, group := range groups {
		count += countCustomsQuestions(group, getGroupCount)
	}
	return count
}

func countCustomsQuestions(text string, getGroupCount func(members int, questionCount []int) int) int {
	lines := strings.Split(text, "\n")

	yesQuestions := make([]int, 26)
	for _, line := range lines {
		letters := []rune(line)

		for _, letter := range letters {
			yesQuestions[int(letter-'a')]++
		}
	}

	return getGroupCount(len(lines), yesQuestions)
}

func getCountForGroup(members int, questionCounts []int) int {
	count := 0
	for _, questinCount := range questionCounts {
		if questinCount > 0 {
			count++
		}
	}
	return count
}

func getCountForGroupV2(members int, questionCounts []int) int {
	count := 0
	for _, questinCount := range questionCounts {
		if questinCount == members {
			count++
		}
	}
	return count
}
