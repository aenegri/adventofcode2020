package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day9 decrypts the XMAS data
func Day9() {
	input := getXMASInput("input/day9.txt")
	part1 := firstInvalidXMAS(25, input)
	part2 := contiguousSum(part1, input)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func getXMASInput(name string) []int {
	text := readFile(name)
	lines := strings.Split(text, "\n")
	input := make([]int, len(lines))
	for i, l := range lines {
		num, _ := strconv.Atoi(l)
		input[i] = num
	}
	return input
}

func firstInvalidXMAS(preamble int, nums []int) int {
	pool := map[int]bool{}

	for i := 0; i < preamble; i++ {
		pool[nums[i]] = true
	}

	for pos := preamble; pos < len(nums); pos++ {
		target := nums[pos]
		valid := false
		for candidate := range pool {
			complement := target - candidate
			if pool[complement] && candidate != complement {
				valid = true
			}
		}

		if !valid {
			return target
		}

		delete(pool, nums[pos-preamble])
		pool[nums[pos]] = true
	}

	return 0
}

func contiguousSum(target int, nums []int) int {
	sum, leftPos, rightPos := nums[0], 0, 0

	for rightPos < len(nums) {
		if sum == target {
			break
		} else if sum > target {
			sum -= nums[leftPos]
			leftPos++
		} else {
			rightPos++
			sum += nums[rightPos]
		}
	}

	min, max := nums[leftPos], nums[leftPos]

	for i := leftPos; i <= rightPos; i++ {
		if nums[i] < min {
			min = nums[i]
		} else if nums[i] > max {
			max = nums[i]
		}
	}

	return min + max
}
