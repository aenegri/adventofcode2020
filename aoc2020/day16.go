package aoc2020

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Day16 resolves the foreign ticket situation
func Day16() {
	sections := strings.Split(readFile("input/day16.txt"), "\n\n")
	rules := parseTrainTicketRules(sections[0])
	ownTicket := parseOwnTrainTicket(sections[1])
	nearbyTickets := parseNearbyTickets(sections[2])

	validTickets, part1 := countTicketErrorRate(nearbyTickets, rules)
	ticketFields := findTicketFields(validTickets, rules)
	part2 := calculateTicketProduct(ownTicket, ticketFields)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func parseTrainTicketRules(text string) []trainTicketRule {
	rulePattern := regexp.MustCompile(`(.+):\s(\d+)-(\d+)\sor\s(\d+)-(\d+)`)
	lines := strings.Split(text, "\n")
	rules := make([]trainTicketRule, len(lines))
	for i, line := range lines {
		match := rulePattern.FindStringSubmatch(line)
		aLo, _ := strconv.Atoi(match[2])
		aHi, _ := strconv.Atoi(match[3])
		bLo, _ := strconv.Atoi(match[4])
		bHi, _ := strconv.Atoi(match[5])
		rules[i] = trainTicketRule{name: match[1], aLo: aLo, aHi: aHi, bLo: bLo, bHi: bHi}
	}
	return rules
}

func parseOwnTrainTicket(text string) trainTicket {
	lines := strings.Split(text, "\n")
	return newTrainTicket(lines[1])
}

func parseNearbyTickets(text string) []trainTicket {
	lines := strings.Split(text, "\n")
	nearby := make([]trainTicket, len(lines))
	for i, line := range lines {
		nearby[i] = newTrainTicket(line)
	}
	return nearby[1:]
}

func findTicketFields(tickets []trainTicket, rules []trainTicketRule) []string {
	possibleRules := [][]string{}
	for i := 0; i < len(tickets[0].fields); i++ {
		indexRules := []string{}
		for _, rule := range rules {
			worksForIndex := true
			for _, ticket := range tickets {
				if !rule.includes(ticket.fields[i]) {
					worksForIndex = false
					break
				}
			}
			if worksForIndex {
				indexRules = append(indexRules, rule.name)
			}
		}
		possibleRules = append(possibleRules, indexRules)
	}

	solved := false
	for !solved {
		solved = true
		for i, fieldRules := range possibleRules {
			if len(fieldRules) != 1 {
				solved = false
				continue
			}
			for j, otherFieldRules := range possibleRules {
				if i == j {
					continue
				}
				idxRemove := -1
				for k, r := range otherFieldRules {
					if r == fieldRules[0] {
						idxRemove = k
						break
					}
				}
				if idxRemove != -1 {
					possibleRules[j] = append(otherFieldRules[:idxRemove], otherFieldRules[idxRemove+1:]...)
				}
			}
		}
	}

	fields := []string{}
	for _, indexRules := range possibleRules {
		fields = append(fields, indexRules[0])
	}

	return fields
}

func calculateTicketProduct(ticket trainTicket, fields []string) int {
	total := 1
	for i, field := range fields {
		if strings.HasPrefix(field, "departure") {
			total *= ticket.fields[i]
		}
	}
	return total
}

func countTicketErrorRate(tickets []trainTicket, rules []trainTicketRule) ([]trainTicket, int) {
	errorRate := 0
	validTickets := []trainTicket{}
	for _, ticket := range tickets {
		ticketErrorRate := ticket.valid(rules)

		if ticketErrorRate == 0 {
			validTickets = append(validTickets, ticket)
		}

		errorRate += ticketErrorRate
	}
	return validTickets, errorRate
}

type trainTicketRule struct {
	name string
	aLo  int
	aHi  int
	bLo  int
	bHi  int
}

func (r trainTicketRule) includes(target int) bool {
	return (target >= r.aLo && target <= r.aHi) || (target >= r.bLo && target <= r.bHi)
}

type trainTicket struct {
	fields []int
}

func newTrainTicket(text string) trainTicket {
	parts := strings.Split(text, ",")
	fields := make([]int, len(parts))
	for j, part := range parts {
		value, _ := strconv.Atoi(part)
		fields[j] = value
	}
	return trainTicket{fields: fields}
}

func (t trainTicket) valid(rules []trainTicketRule) int {
	errorRate := 0
	for _, f := range t.fields {
		validField := false
		for _, r := range rules {
			if r.includes(f) {
				validField = true
				break
			}
		}
		if !validField {
			errorRate += f
		}
	}
	return errorRate
}
