package aoc2020

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Day4 counts valid passports
func Day4() {
	passportData := readFile("input/day4.txt")
	part1 := countValidPassports(passportData, func(_, _ string) bool { return true })
	part2 := countValidPassports(passportData, validatePassportField)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func countValidPassports(text string, fieldValidator func(k, v string) bool) int {
	requiredFields := map[string]bool{
		"byr": true,
		"iyr": true,
		"eyr": true,
		"hgt": true,
		"hcl": true,
		"ecl": true,
		"pid": true,
	}

	count := 0
	rawPassports := strings.Split(text, "\n\n")

	for _, passport := range rawPassports {
		fields := strings.Fields(passport)
		includedFields := map[string]bool{}

		for _, field := range fields {
			parts := strings.Split(field, ":")

			if fieldValidator(parts[0], parts[1]) {
				includedFields[parts[0]] = true
			}
		}

		valid := true
		for requiredField := range requiredFields {
			if !includedFields[requiredField] {
				valid = false
				break
			}
		}

		if valid {
			count++
		}
	}

	return count
}

func validatePassportField(key, value string) bool {
	heightMatcher := regexp.MustCompile(`^(\d{2,3})(\w{2})$`)
	hairMatcher := regexp.MustCompile(`^#[0-9a-f]{6}$`)
	eyeColors := map[string]bool{"amb": true, "blu": true, "brn": true, "gry": true, "grn": true, "hzl": true, "oth": true}
	pidMatcher := regexp.MustCompile(`^\d{9}$`)
	switch key {
	case "byr":
		year, err := strconv.Atoi(value)
		if err != nil {
			return false
		}
		return year >= 1920 && year <= 2002
	case "iyr":
		year, err := strconv.Atoi(value)
		if err != nil {
			return false
		}
		return year >= 2010 && year <= 2020
	case "eyr":
		year, err := strconv.Atoi(value)
		if err != nil {
			return false
		}
		return year >= 2020 && year <= 2030
	case "hgt":
		matches := heightMatcher.FindStringSubmatch(value)
		if len(matches) < 1 {
			return false
		}
		num, err := strconv.Atoi(matches[1])
		if err != nil {
			return false
		}
		unit := matches[2]

		if unit == "in" {
			return num >= 59 && num <= 76
		} else if unit == "cm" {
			return num >= 150 && num <= 193
		}
		return false
	case "hcl":
		return hairMatcher.MatchString(value)
	case "ecl":
		return eyeColors[value]
	case "pid":
		return pidMatcher.MatchString(value)
	}

	return false
}
