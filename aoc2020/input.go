package aoc2020

import (
	"io/ioutil"
)

func readFile(name string) string {
	bytes, err := ioutil.ReadFile(name)

	if err != nil {
		panic(err)
	}

	return string(bytes)
}
