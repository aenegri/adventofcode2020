package aoc2020

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

// Day11 runs the game of seats for the ferry ride
func Day11() {
	seedBase, h, w := getSeatingChart(readFile("input/day11.txt"))

	seed1 := copyChart(seedBase)
	seed2 := copyChart(seedBase)
	addAdjacentSeats(seed1)
	addAdjacentSeats(seed2)

	seed3 := copyChart(seedBase)
	seed4 := copyChart(seedBase)
	addVisibleSeats(seed3, h, w)
	addVisibleSeats(seed4, h, w)

	startSync := time.Now()
	part1Sync := gameOfSeats(seed1, runSeatGenSync, 4, 64)
	syncTime := time.Now().Sub(startSync)

	startConcurrent := time.Now()
	part1Concurrent := gameOfSeats(seed2, runSeatGen, 4, 0)
	concurrentTime := time.Now().Sub(startConcurrent)

	startPart2Sync := time.Now()
	part2Sync := gameOfSeats(seed3, runSeatGenSync, 5, 0)
	part2SyncTime := time.Now().Sub(startPart2Sync)

	startPart2Concurrent := time.Now()
	part2Concurrent := gameOfSeats(seed4, runSeatGenSync, 5, 64)
	part2ConcurrentTime := time.Now().Sub(startPart2Concurrent)

	fmt.Printf("Part 1 (Sync): %d Duration: %s\n", part1Sync, syncTime)
	fmt.Printf("Part 1 (Concurrent): %d Duration: %s\n", part1Concurrent, concurrentTime)
	fmt.Printf("Part 2 (Sync): %d Duration: %s\n", part2Sync, part2SyncTime)
	fmt.Printf("Part 2 (Concurrent): %d Duration: %s\n", part2Concurrent, part2ConcurrentTime)
}

func getSeatingChart(text string) (map[point]*seat, int, int) {
	chart := map[point]*seat{}
	lines := strings.Split(text, "\n")
	for row, line := range lines {
		letters := []rune(line)
		for col, letter := range letters {
			if letter == 'L' {
				pt := point{row: row, col: col}
				chart[pt] = &seat{adjacent: []*seat{}}

			}
		}
	}
	return chart, len(lines), len(lines[0])
}

func addAdjacentSeats(chart map[point]*seat) {
	for pt, s := range chart {
		for row := pt.row - 1; row <= pt.row+1; row++ {
			for col := pt.col - 1; col <= pt.col+1; col++ {
				if row == pt.row && col == pt.col {
					continue // don't add ref to self
				}
				if adj, ok := chart[point{row: row, col: col}]; ok {
					s.adjacent = append(s.adjacent, adj)
				}
			}
		}
	}
}

func addVisibleSeats(chart map[point]*seat, height, width int) {
	for pt, s := range chart {
		dirX, dirY := -1, -1
		for dirX < 2 {
			for dirY < 2 {
				if dirX == 0 && dirY == 0 {
					dirY++
					continue
				}
				target := point{row: pt.row + dirY, col: pt.col + dirX}
				for (target.row >= 0 && target.row < height) &&
					(target.col >= 0 && target.col < width) {
					if chart[target] != nil {
						s.adjacent = append(s.adjacent, chart[target])
						break
					}
					target.row += dirY
					target.col += dirX
				}
				dirY++
			}
			dirY = -1
			dirX++
		}
	}
}

func copyChart(chart map[point]*seat) map[point]*seat {
	cp := map[point]*seat{}
	for pt := range chart {
		cp[pt] = &seat{adjacent: []*seat{}}
	}
	return cp
}

func gameOfSeats(seed map[point]*seat, fn ferrySeatSimulator, threshold, interval int) int {
	isOdd, lastCount, count := true, 0, runSeatGen(seed, false, threshold, interval)
	for {
		count = fn(seed, isOdd, threshold, interval)

		if count == lastCount {
			return count
		}

		lastCount = count
		isOdd = !isOdd

	}
}

type point struct {
	row int
	col int
}

type seat struct {
	state0   bool
	state1   bool
	adjacent []*seat
}

func (s *seat) next(isOdd bool, threshold int) bool {
	cnt := 0

	for _, adj := range s.adjacent {
		if (isOdd && adj.state0) || (!isOdd && adj.state1) {
			cnt++
		}
	}

	currState := s.state1
	if isOdd {
		currState = s.state0
	}

	nextState := currState
	if cnt >= threshold {
		nextState = false
	} else if cnt == 0 {
		nextState = true
	}

	if isOdd {
		s.state1 = nextState
	} else {
		s.state0 = nextState
	}

	return nextState
}

type ferrySeatSimulator func(chart map[point]*seat, isOdd bool, threshold, interval int) int

func runSeatGenSync(chart map[point]*seat, isOdd bool, threshold, interval int) int {
	count := 0
	for _, s := range chart {
		if s.next(isOdd, threshold) {
			count++
		}
	}
	return count
}

func runSeatGen(chart map[point]*seat, isOdd bool, threshold, interval int) int {
	countStream := make(chan int)
	resultStream := make(chan int)

	go func() {
		defer close(resultStream)
		count := 0
		for {
			select {
			case cnt, ok := <-countStream:
				if !ok {
					resultStream <- count
					return
				}
				count += cnt
			}
		}
	}()

	wg := &sync.WaitGroup{}

	idx := 0

	group := []*seat{}
	for _, s := range chart {
		if idx == interval {
			wg.Add(1)
			go runSeatSubgroup(wg, countStream, group, isOdd, threshold)

			group = []*seat{}
			idx = 0
		}

		group = append(group, s)
		idx++
	}

	if len(group) > 0 {
		wg.Add(1)
		go runSeatSubgroup(wg, countStream, group, isOdd, threshold)
	}

	wg.Wait()
	close(countStream)

	return <-resultStream
}

func runSeatSubgroup(wg *sync.WaitGroup, countStream chan<- int, seats []*seat, isOdd bool, threshold int) {
	defer wg.Done()
	cnt := 0
	for _, currSeat := range seats {
		if currSeat.next(isOdd, threshold) {
			cnt++
		}
	}
	countStream <- cnt
}
