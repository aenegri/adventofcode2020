package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day12 navigates the ferry
func Day12() {
	lines := strings.Split(readFile("input/day12.txt"), "\n")

	instructions := make([]instruction, len(lines))

	for i, line := range lines {
		v, _ := strconv.Atoi(line[1:])
		instructions[i] = instruction{action: []rune(line)[0], val: v}
	}

	part1 := manhattanDist(instructions)
	part2 := manhattanDistV2(instructions)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

type instruction struct {
	action rune
	val    int
}

func manhattanDist(steps []instruction) int {
	posX, posY, angle := 0, 0, 0 // angle 0 is east
	compass := map[int]rune{0: 'E', 90: 'S', 180: 'W', 270: 'N'}
	for _, step := range steps {
		if step.action == 'F' {
			step.action = compass[angle]
		}

		if step.action == 'N' {
			posY += step.val
		} else if step.action == 'E' {
			posX += step.val
		} else if step.action == 'S' {
			posY -= step.val
		} else if step.action == 'W' {
			posX -= step.val
		} else if step.action == 'R' {
			angle = (angle + step.val) % 360
		} else if step.action == 'L' {
			angle = (angle + (360 - step.val)) % 360
		} else {
			panic("Unsupported action: " + string(step.action))
		}
	}

	if posX < 0 {
		posX = -posX
	}
	if posY < 0 {
		posY = -posY
	}

	return posX + posY
}

func manhattanDistV2(steps []instruction) int {
	posX, posY, wayX, wayY := 0, 0, 10, 1 // waypoint pos is relative to ship

	for _, step := range steps {
		if step.action == 'F' {
			posX += wayX * step.val
			posY += wayY * step.val
		} else if step.action == 'N' {
			wayY += step.val
		} else if step.action == 'E' {
			wayX += step.val
		} else if step.action == 'S' {
			wayY -= step.val
		} else if step.action == 'W' {
			wayX -= step.val
		} else if step.action == 'L' || step.action == 'R' {
			deg := (step.val % 360)
			if step.action == 'L' {
				deg = 360 - deg
			}
			if deg == 90 {
				wayX, wayY = wayY, -wayX
			} else if deg == 180 {
				wayX, wayY = -wayX, -wayY
			} else if deg == 270 {
				wayX, wayY = -wayY, wayX
			}
		} else {
			panic("Unsupported action: " + string(step.action))
		}

	}

	if posX < 0 {
		posX = -posX
	}
	if posY < 0 {
		posY = -posY
	}

	return posX + posY
}
