package aoc2020

import (
	"fmt"
	"strings"
)

// Day3 counts trees the toboggan will encounter
func Day3() {
	f := newForest("input/day3.txt")
	part1 := countTrees(1, 3, f)
	part2 := countTreesForSlopes([][]int{
		{1, 1},
		{1, 3},
		{1, 5},
		{1, 7},
		{2, 1},
	}, f)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func countTrees(deltaX, deltaY int, f *forest) int {
	x, y := 0, 0
	count := 0
	for f.inForest(x) {
		if !f.isEmpty(x, y) {
			count++
		}
		x += deltaX
		y += deltaY
	}
	return count
}

func countTreesForSlopes(slopes [][]int, f *forest) int {
	total := 1
	for _, slope := range slopes {
		total *= countTrees(slope[0], slope[1], f)
	}
	return total
}

type forest struct {
	terrain [][]bool
}

func newForest(path string) *forest {
	text := readFile(path)
	lines := strings.Split(text, "\n")

	terrain := make([][]bool, len(lines))

	for i, line := range lines {
		letters := []rune(line)
		terrain[i] = make([]bool, len(letters))
		for j, letter := range letters {
			terrain[i][j] = letter == '.'
		}
	}
	return &forest{
		terrain: terrain,
	}
}

func (f *forest) isEmpty(x, y int) bool {
	return f.terrain[x][y%len(f.terrain[0])]
}

func (f *forest) inForest(x int) bool {
	return x < len(f.terrain)
}
