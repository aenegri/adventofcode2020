package aoc2020

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Day14 initializes the ferry's docking program
func Day14() {
	ops := parsePortOperations(readFile("input/day14.txt"))
	part1 := dockFerry(ops)
	part2 := dockFerryV2(ops)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func dockFerry(ops []portOp) int {
	isolationMask, bitMask := 0, 0
	memory := map[int]int{}

	for _, op := range ops {
		if op.op == "mask" {
			isolationMask, bitMask = 0, 0
			for i := 0; i < len(op.arg); i++ {
				rawBit := op.arg[len(op.arg)-i-1 : len(op.arg)-i]

				if rawBit == "X" {
					isolationMask = isolationMask | (1 << i)
				} else {
					bit, _ := strconv.Atoi(rawBit)
					bitMask = bitMask | (bit << i)
				}
			}
		} else if op.op == "set" {
			val, _ := strconv.Atoi(op.arg)
			val = (val & isolationMask) | bitMask
			memory[op.addr] = val
		}
	}

	return getMemoryTotal(memory)
}

func dockFerryV2(ops []portOp) int {
	isolationMask := 0
	bitMasks := []int{0}
	memory := map[int]int{}

	for _, op := range ops {
		if op.op == "mask" {
			isolationMask = 0
			bitMasks = []int{0}
			for i := 0; i < len(op.arg); i++ {
				rawBit := op.arg[len(op.arg)-i-1 : len(op.arg)-i]
				if rawBit == "X" {

					newMasks := make([]int, len(bitMasks))
					for idx, mask := range bitMasks {
						newMasks[idx] = mask | (1 << i)
					}
					bitMasks = append(bitMasks, newMasks...)
				} else if rawBit == "1" {
					isolationMask = isolationMask | (1 << i)
					for idx, mask := range bitMasks {
						bitMasks[idx] = mask | (1 << i)
					}
				} else {
					isolationMask = isolationMask | (1 << i)
				}
			}
		} else if op.op == "set" {
			val, _ := strconv.Atoi(op.arg)
			for _, mask := range bitMasks {
				addr := (op.addr & isolationMask) | mask
				memory[addr] = val
			}
		}
	}

	return getMemoryTotal(memory)
}

func getMemoryTotal(memory map[int]int) int {
	total := 0
	for _, val := range memory {
		total += val
	}
	return total
}

type portOp struct {
	op   string
	addr int
	arg  string
}

func parsePortOperations(text string) []portOp {
	lines := strings.Split(text, "\n")
	ops := make([]portOp, len(lines))
	addrPattern := regexp.MustCompile(`\[(\d+)\]`)
	for i, line := range lines {
		parts := strings.Split(line, " = ")
		op := portOp{arg: parts[1]}
		if strings.Contains(parts[0], "[") {
			op.op = "set"
			rawAddress := addrPattern.FindStringSubmatch(parts[0])[1]
			addr, _ := strconv.Atoi(rawAddress)
			op.addr = addr
		} else {
			op.op = "mask"
		}
		ops[i] = op
	}
	return ops
}
