package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day2 checks for valid passwords according to the policy
func Day2() {
	entries := parsePasswordList("input/day2.txt")

	part1 := countValidPasswords(entries, valid)
	part2 := countValidPasswords(entries, validV2)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func countValidPasswords(entries []passwordEntry, alg func(e passwordEntry) bool) int {
	count := 0

	for _, e := range entries {
		if alg(e) {
			count++
		}
	}

	return count
}

func valid(e passwordEntry) bool {
	occurences := 0
	for index := 0; index < len(e.password); index++ {
		segment := e.password[index : index+1]

		if segment == e.char {
			occurences++
		}
	}
	return occurences >= e.i && occurences <= e.j
}

func validV2(e passwordEntry) bool {
	segA := e.password[e.i-1 : e.i]
	segB := e.password[e.j-1 : e.j]

	return (segA == e.char) != (segB == e.char)
}

type passwordEntry struct {
	password string
	i        int
	j        int
	char     string
}

func parsePasswordList(name string) []passwordEntry {
	text := readFile(name)

	lines := strings.Split(text, "\n")

	entries := make([]passwordEntry, len(lines))

	for index, line := range lines {
		parts := strings.Split(line, " ")
		rangeParts := strings.Split(parts[0], "-")
		i, _ := strconv.Atoi(rangeParts[0])
		j, _ := strconv.Atoi(rangeParts[1])

		entries[index] = passwordEntry{
			password: parts[2],
			i:        i,
			j:        j,
			char:     parts[1][:len(parts[1])-1],
		}
	}

	return entries
}
