package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day1 fixes the expense report
func Day1() {
	nums := parseExpenseReport("input/day1.txt")
	part1 := fixExpenseReport(nums)
	part2 := fixExpenseReportV2(nums)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func parseExpenseReport(name string) []int {
	text := readFile(name)

	lines := strings.Split(text, "\n")

	nums := make([]int, len(lines))

	for index, l := range lines {
		v, err := strconv.Atoi(l)

		if err != nil {
			panic(err)
		}

		nums[index] = v
	}

	return nums
}

func fixExpenseReport(nums []int) int {
	target := 2020
	possibleEntries := map[int]bool{}

	for _, num := range nums {
		if !possibleEntries[num] {
			possibleEntries[target-num] = true
			continue
		}

		complement := target - num

		return num * complement
	}

	return -1
}

func fixExpenseReportV2(nums []int) int {
	target := 2020
	numCounts := map[int]int{}

	for _, num := range nums {
		numCounts[num]++
	}

	for i, a := range nums {
		for j := i + 1; j < len(nums); j++ {
			b := nums[j]
			c := target - a - b

			count := numCounts[c]

			if a == c {
				count--
			}
			if b == c {
				count--
			}
			if count < 1 {
				continue
			}

			return a * b * c
		}
	}

	return -1
}
