package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day10 chains together adapters rated for differing jolts
func Day10() {
	adapters := getAdapterSet(readFile("input/day10.txt"))
	part1 := <-adapterJoltDiff(adapters)
	part2 := <-adapterJoltCombinations(adapters)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func getAdapterSet(text string) map[int]bool {
	adapters := map[int]bool{}
	lines := strings.Split(text, "\n")
	for _, l := range lines {
		jolts, err := strconv.Atoi(l)

		if err != nil {
			panic(err)
		}
		adapters[jolts] = true
	}
	return adapters
}

func adapterJoltDiff(adapters map[int]bool) <-chan int {
	done := make(chan bool)
	diffStream := make(chan int)
	resultStream := make(chan int)

	go func() {
		defer close(diffStream)
		defer close(resultStream)

		diffs := make([]int, 4)
		for {
			select {
			case diff, ok := <-diffStream:
				if !ok {
					return
				}
				diffs[diff]++
			case <-done:
				resultStream <- diffs[1] * diffs[3]
			}
		}
	}()

	go func() {
		i := 0
		for {
			if adapters[i+1] {
				diffStream <- 1
				i++
			} else if adapters[i+2] {
				diffStream <- 2
				i += 2
			} else if adapters[i+3] {
				diffStream <- 3
				i += 3
			} else {
				diffStream <- 3
				close(done)
				return
			}
		}
	}()

	return resultStream
}

func adapterJoltCombinations(adapters map[int]bool) <-chan int {
	resultStream := make(chan int)

	go func() {
		defer close(resultStream)
		result := <-adapterJoltDFS(0, adapters, map[int]int{})
		resultStream <- result
	}()

	return resultStream
}

func adapterJoltDFS(adapter int, adapters map[int]bool, memo map[int]int) <-chan int {
	resultStream := make(chan int)

	go func() {
		defer close(resultStream)
		if v, ok := memo[adapter]; ok {
			resultStream <- v
			return
		}

		total := 0

		hasChild := false
		for diff := 1; diff <= 3; diff++ {
			if adapters[adapter+diff] {
				hasChild = true
				child := adapterJoltDFS(adapter+diff, adapters, memo)
				total += <-child
			}
		}

		if !hasChild {
			total = 1
		}

		memo[adapter] = total
		resultStream <- total
	}()
	return resultStream
}
