package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

func Day18() {
	equations := readFile("input/day18.txt")
	part1 := mathEvalAll(equations, mathEval)
	part2 := mathEvalAll(equations, mathEvalV2)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func mathEvalAll(text string, evaluator mathEvaluator) int {
	lines := strings.Split(text, "\n")
	total := 0
	for _, line := range lines {
		total += evaluator(line, 0)
	}
	return total
}

type mathEvaluator func(exp string, level int) int

func mathEval(expression string, level int) int {
	operator := ""
	operandCount := 0
	operandA, operandB := 0, 0
	i := 0
	for i < len(expression) {
		token := expression[i : i+1]

		switch token {
		case " ":
			break
		case "+", "*":
			operator = token
		case "(":
			innerResult := 0
			j := getSubexpression(expression, i+1, token)
			innerResult = mathEval(expression[i+1:j], level+1)

			if operandCount == 1 {
				operandB = innerResult
			} else {
				operandA = innerResult
			}
			i = j
			operandCount++
		case ")":
			if level != 0 {
				return operandA
			}
		default:
			if operandCount == 1 {
				operandB, _ = strconv.Atoi(token)
			} else {
				operandA, _ = strconv.Atoi(token)
			}
			operandCount++
		}

		if operandCount == 2 {
			if operator == "+" {
				operandA += operandB
			} else if operator == "*" {
				operandA *= operandB
			}
			operandCount--
		}

		i++
	}

	return operandA
}

func mathEvalV2(expression string, level int) int {
	operator := ""
	operandCount := 0
	operandA, operandB := 0, 0
	i := 0
	for i < len(expression) {
		token := expression[i : i+1]

		switch token {
		case " ":
			break
		case "+":
			operator = token
		case "*":
			operator = token
			fallthrough
		case "(":
			innerResult := 0
			j := getSubexpression(expression, i+1, token)
			innerResult = mathEvalV2(expression[i+1:j], level+1)

			if operandCount == 1 {
				operandB = innerResult
			} else {
				operandA = innerResult
			}
			i = j
			operandCount++
		case ")":
			if level != 0 {
				return operandA
			}
		default:
			if operandCount == 1 {
				operandB, _ = strconv.Atoi(token)
			} else {
				operandA, _ = strconv.Atoi(token)
			}
			operandCount++
		}

		if operandCount == 2 {
			if operator == "+" {
				operandA += operandB
			} else if operator == "*" {
				operandA *= operandB
			}
			operandCount--
		}

		i++
	}

	return operandA
}

func getSubexpression(expression string, start int, target string) int {
	end := len(expression)
	if target == "(" {
		depth := 1
		for i := start; i < len(expression); i++ {
			token := expression[i : i+1]
			if token == "(" {
				depth++
			} else if token == ")" {
				depth--
			}
			if depth == 0 {
				end = i
				break
			}
		}
	} else {
		depth := 0
		for i := start; i < len(expression); i++ {
			token := expression[i : i+1]
			if token == target && depth == 0 {
				end = i - 1
				break
			} else if token == "(" {
				depth++
			} else if token == ")" {
				depth--
			}
		}
	}

	return end
}
