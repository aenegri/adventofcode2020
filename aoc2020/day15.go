package aoc2020

import "fmt"

// Day15 runs the memory game while waiting at the airport
func Day15() {
	seed := []int{19, 20, 14, 0, 9, 1}
	part1 := memoryGame(seed, 2020)
	part2 := memoryGame(seed, 30000000)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func memoryGame(seed []int, rounds int) int {
	round, lastNum, num := 1, 0, 0
	record := map[int]int{}

	for round <= rounds {
		if round <= len(seed) {
			num = seed[round-1]
		} else if lastRound, ok := record[lastNum]; ok {
			num = round - lastRound
		} else {
			num = 0
		}

		record[lastNum] = round
		lastNum = num
		round++
	}

	return lastNum
}
