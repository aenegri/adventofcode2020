package aoc2020

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Day7 finds how many ways you can have a shiny gold bag and
// how many bags are within a shiny gold bag
func Day7() {
	graph := buildLuggageGraph(readFile("input/day7.txt"))

	part1 := graph.countPaths("shiny gold")
	part2 := graph.countBags("shiny gold")

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func buildLuggageGraph(text string) *luggageGraph {
	graph := &luggageGraph{
		colors: map[string]*luggageRule{},
	}
	rootPattern := regexp.MustCompile(`^\w+\s\w+`)
	childPattern := regexp.MustCompile(`(\d)+\s(\w+\s\w+)`)
	lines := strings.Split(text, "\n")
	for _, line := range lines {
		parts := strings.Split(line, "contain")
		rootMatch := rootPattern.FindString(parts[0])
		if strings.Contains(parts[1], "no other") {
			graph.add(rootMatch, []string{}, []int{})
		} else {
			rawChildren := strings.Split(parts[1], ",")
			childColors := make([]string, len(rawChildren))
			childQty := make([]int, len(rawChildren))
			for idx, rawChild := range rawChildren {
				childMatches := childPattern.FindStringSubmatch(rawChild)
				luggageQty, _ := strconv.Atoi(childMatches[1])
				luggageColor := childMatches[2]
				childColors[idx] = luggageColor
				childQty[idx] = luggageQty
			}

			graph.add(rootMatch, childColors, childQty)
		}
	}

	return graph
}

type luggageRule struct {
	color    string
	contains map[string]int
}

type luggageGraph struct {
	colors map[string]*luggageRule
}

func (g *luggageGraph) add(color string, childColors []string, childCounts []int) {
	rule := &luggageRule{
		color:    color,
		contains: map[string]int{},
	}
	for idx, c := range childColors {
		rule.contains[c] = childCounts[idx]
	}
	g.colors[color] = rule
}

func (g *luggageGraph) countPaths(target string) int {
	visited := map[string]bool{}
	containsTarget := map[string]bool{}
	for _, rule := range g.colors {
		g.dfs(target, rule, visited, containsTarget)
	}

	count := 0
	for c := range containsTarget {
		if c != target {
			count++
		}
	}
	return count
}

func (g *luggageGraph) dfs(target string, rule *luggageRule, visited, containsTarget map[string]bool) bool {
	if rule.color == target || containsTarget[rule.color] {
		containsTarget[rule.color] = true
		return true
	} else if visited[rule.color] {
		return false
	}

	visited[rule.color] = true

	for childColor := range rule.contains {
		childRule := g.colors[childColor]
		found := g.dfs(target, childRule, visited, containsTarget)

		if found {
			containsTarget[rule.color] = true
			return true
		}
	}

	return false
}

func (g *luggageGraph) countBags(target string) int {
	return g.countBagsDFS(target, map[string]int{})
}

func (g *luggageGraph) countBagsDFS(target string, memo map[string]int) int {
	if len(g.colors[target].contains) == 0 {
		return 0
	} else if memo[target] != 0 {
		return memo[target]
	}

	total := 0
	for color, count := range g.colors[target].contains {
		subCount := g.countBagsDFS(color, memo)
		memo[color] = subCount
		total += count + count*g.countBagsDFS(color, memo)
	}
	return total
}

func (g *luggageGraph) print() {
	for c, rule := range g.colors {
		fmt.Printf("%s: %v\n", c, rule)
	}
}
