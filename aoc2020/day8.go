package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day8 finds the boot loop in the game console
func Day8() {
	console := new(readFile("input/day8.txt"))
	part1 := console.detectLoop()
	console.reset()
	part2 := console.fixLoop()

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

type gameConsole struct {
	ops []bootOp
	pos int
	acc int
}

func new(rawInstructions string) *gameConsole {
	console := &gameConsole{
		pos: 0,
		acc: 0,
	}

	lines := strings.Split(rawInstructions, "\n")
	ops := make([]bootOp, len(lines))
	for i, line := range lines {
		signature := strings.Split(line, " ")
		arg, err := strconv.Atoi(signature[1])

		if err != nil {
			panic(err)
		}

		ops[i] = bootOp{op: signature[0], arg: arg}
	}
	console.ops = ops
	return console
}

func (c *gameConsole) step() {
	op := c.ops[c.pos]

	switch op.op {
	case "acc":
		c.acc += op.arg
		c.pos++
	case "jmp":
		c.pos += op.arg
	case "nop":
		c.pos++
	}
}

func (c *gameConsole) detectLoop() int {
	visitedOps := map[int]bool{}

	for c.pos < len(c.ops) {
		if visitedOps[c.pos] {
			return c.acc
		}
		visitedOps[c.pos] = true
		c.step()
	}

	return c.acc
}

func (c *gameConsole) fixLoop() int {
	for i := 0; i < len(c.ops); i++ {
		currOp := c.ops[i]
		switch currOp.op {
		case "jmp":
			c.ops[i] = bootOp{op: "nop", arg: currOp.arg}
		case "nop":
			c.ops[i] = bootOp{op: "jmp", arg: currOp.arg}
		default:
			continue
		}
		c.detectLoop()

		if c.pos >= len(c.ops) {
			return c.acc
		}

		c.ops[i] = currOp
		c.reset()
	}
	return 0
}

func (c *gameConsole) reset() {
	c.pos = 0
	c.acc = 0
}

type bootOp struct {
	op  string
	arg int
}
