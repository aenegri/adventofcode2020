package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day13 solves day 13
func Day13() {
	text := readFile("input/day13.txt")
	lines := strings.Split(text, "\n")
	start, _ := strconv.Atoi(lines[0])
	busParts := strings.Split(lines[1], ",")
	busIDs := []int{}
	for _, busPart := range busParts {
		id, err := strconv.Atoi(busPart)
		if err == nil {
			busIDs = append(busIDs, id)
		} else {
			busIDs = append(busIDs, -1)
		}
	}

	part1 := getEarliestBus(start, busIDs)

	part2 := getContinuousBusSchedule(busIDs)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func getEarliestBus(start int, ids []int) int {
	earliestBus := ids[0]
	earliestTime := start * 2

	for _, bus := range ids {
		if bus < 0 {
			continue
		}
		multiple := start / bus
		next := (multiple + 1) * bus
		if next < earliestTime {
			earliestTime = next
			earliestBus = bus
		}
	}
	return (earliestTime - start) * earliestBus
}

func getContinuousBusSchedule(ids []int) int {
	timestamp, step := 0, 1
	for idx, id := range ids {
		if id < 0 {
			continue
		}
		for (timestamp+idx)%id != 0 {
			timestamp += step
		}
		step *= id
	}

	return timestamp
}
