package aoc2020

import (
	"fmt"
	"strconv"
	"strings"
)

// Day5 solves day 5
func Day5() {
	text := readFile("input/day5.txt")
	part1 := getHighestSeatID(text)
	part2 := findEmptySeat(text)

	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}

func getHighestSeatID(text string) int {
	tickets := strings.Split(text, "\n")
	max := 0
	for _, t := range tickets {
		seatID := getSeatID(getSeatPosition(t))
		if seatID > max {
			max = seatID
		}
	}
	return max
}

func findEmptySeat(text string) int {
	occupied := make([][]bool, 128)
	tickets := strings.Split(text, "\n")
	for _, t := range tickets {
		seatPos := getSeatPosition(t)
		row := occupied[seatPos[0]]

		if row == nil {
			occupied[seatPos[0]] = make([]bool, 8)
			row = occupied[seatPos[0]]
		}
		row[seatPos[1]] = true
	}

	for i, row := range occupied {
		for j, hasSeat := range row {
			if !hasSeat && i > 7 && i < 113 {
				return getSeatID([]int{i, j})
			}
		}
	}

	return 0
}

func getSeatID(pos []int) int {
	return pos[0]*8 + pos[1]
}

func getSeatPosition(pos string) []int {
	rowBinary := make([]rune, 7)
	colBinary := make([]rune, 3)

	letters := []rune(pos)

	for index := 0; index < 7; index++ {
		if letters[index] == 'B' {
			rowBinary[index] = '1'
		} else {
			rowBinary[index] = '0'
		}
	}

	for index := 7; index < 10; index++ {
		if letters[index] == 'R' {
			colBinary[index-7] = '1'
		} else {
			colBinary[index-7] = '0'
		}
	}

	row, err := strconv.ParseInt(string(rowBinary), 2, 64)

	if err != nil {
		panic(err)
	}

	col, err := strconv.ParseInt(string(colBinary), 2, 64)

	if err != nil {
		panic(err)
	}

	return []int{int(row), int(col)}
}
