package aoc2020

import (
	"fmt"
	"strings"
)

func Day17() {
	text := readFile("input/day17.txt")
	energySource := newPocketDimension(text)
	energySource.run(6)
	energSourceV2 := newPocketDimensionV2(text)
	energSourceV2.run(6)

	fmt.Printf("Part 1: %d\n", energySource.activeCount)
	fmt.Printf("Part 2: %d\n", energSourceV2.activeCount)
}

type point3d struct {
	x int
	y int
	z int
}

type pocketDimension struct {
	activeCount int
	active      []point3d
	grid        map[point3d]bool
}

func newPocketDimension(text string) *pocketDimension {
	lines := strings.Split(text, "\n")
	activePoints := []point3d{}
	grid := map[point3d]bool{}
	for i, line := range lines {
		for j := 0; j < len(line); j++ {
			pt := point3d{x: j, y: i, z: 0}
			active := line[j:j+1] == "#"
			grid[pt] = active
			if active {
				activePoints = append(activePoints, pt)
			}
		}
	}
	return &pocketDimension{
		active: activePoints,
		grid:   grid,
	}
}

func (d *pocketDimension) run(n int) {
	for cycle := 0; cycle < n; cycle++ {
		d.activeCount = 0
		nextActive := []point3d{}
		nextGrid := map[point3d]bool{}

		visited := map[point3d]bool{}
		for _, p := range d.active {
			for x := p.x - 1; x < p.x+2; x++ {
				for y := p.y - 1; y < p.y+2; y++ {
					for z := p.z - 1; z < p.z+2; z++ {
						currPt := point3d{x: x, y: y, z: z}

						if visited[currPt] {
							continue
						}

						visited[currPt] = true

						active := d.nextCubeState(x, y, z)
						nextGrid[currPt] = active

						if active {
							nextActive = append(nextActive, currPt)
							d.activeCount++
						}
					}
				}
			}
		}

		d.active = nextActive
		d.grid = nextGrid
	}
}

func (d *pocketDimension) nextCubeState(x, y, z int) bool {
	count := 0
	for i := x - 1; i < x+2; i++ {
		for j := y - 1; j < y+2; j++ {
			for k := z - 1; k < z+2; k++ {
				if i == x && j == y && k == z {
					continue
				}
				if d.grid[point3d{x: i, y: j, z: k}] {
					count++
				}
			}
		}
	}
	nextState := d.grid[point3d{x: x, y: y, z: z}]
	if nextState && !(count == 2 || count == 3) {
		nextState = false
	} else if !nextState && count == 3 {
		nextState = true
	}
	return nextState
}

type point4d struct {
	x int
	y int
	z int
	w int
}

type pocketDimensionV2 struct {
	activeCount int
	active      []point4d
	grid        map[point4d]bool
}

func newPocketDimensionV2(text string) *pocketDimensionV2 {
	lines := strings.Split(text, "\n")
	activePoints := []point4d{}
	grid := map[point4d]bool{}
	for i, line := range lines {
		for j := 0; j < len(line); j++ {
			pt := point4d{x: j, y: i, z: 0, w: 0}
			active := line[j:j+1] == "#"
			grid[pt] = active
			if active {
				activePoints = append(activePoints, pt)
			}
		}
	}
	return &pocketDimensionV2{
		active: activePoints,
		grid:   grid,
	}
}

func (d *pocketDimensionV2) run(n int) {
	for cycle := 0; cycle < n; cycle++ {
		d.activeCount = 0
		nextActive := []point4d{}
		nextGrid := map[point4d]bool{}

		visited := map[point4d]bool{}
		for _, p := range d.active {
			for x := p.x - 1; x < p.x+2; x++ {
				for y := p.y - 1; y < p.y+2; y++ {
					for z := p.z - 1; z < p.z+2; z++ {
						for w := p.w - 1; w < p.w+2; w++ {
							currPt := point4d{x: x, y: y, z: z, w: w}

							if visited[currPt] {
								continue
							}

							visited[currPt] = true

							active := d.nextCubeState(x, y, z, w)
							nextGrid[currPt] = active

							if active {
								nextActive = append(nextActive, currPt)
								d.activeCount++
							}
						}
					}
				}
			}
		}

		d.active = nextActive
		d.grid = nextGrid
	}
}

func (d *pocketDimensionV2) nextCubeState(x, y, z, w int) bool {
	count := 0
	for i := x - 1; i < x+2; i++ {
		for j := y - 1; j < y+2; j++ {
			for k := z - 1; k < z+2; k++ {
				for l := w - 1; l < w+2; l++ {
					if i == x && j == y && k == z && l == w {
						continue
					}
					if d.grid[point4d{x: i, y: j, z: k, w: l}] {
						count++
					}
				}

			}
		}
	}
	nextState := d.grid[point4d{x: x, y: y, z: z, w: w}]
	if nextState && !(count == 2 || count == 3) {
		nextState = false
	} else if !nextState && count == 3 {
		nextState = true
	}
	return nextState
}
